package payroll;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * This class is responsible for... <undocumented-class>
 */
public interface OrderRepository extends JpaRepository<Order, Long> {
}
