package payroll;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PayrolApplication {

    public static void main(String[] args) {
        SpringApplication.run(PayrolApplication.class, args);
    }
}

class EmployeeNotFoundException extends RuntimeException {

    public EmployeeNotFoundException(long emmploueeId) {
        super("could not find user '" + emmploueeId + "'.");
    }
}

class OrderNotFoundException extends RuntimeException {

    public OrderNotFoundException(long orderId) {
        super("could not find user '" + orderId + "'.");
    }
}

